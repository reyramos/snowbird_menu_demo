/**
 * # Global Directives
 *
 * Directives used in the application
 */

/**
 * Ensures text entered is numberical.
 */

angular.module('app').directive
('numbersOnly'
	, function () {
		var directive =
		{ link: function (scope, elm, attrs) {
			elm.bind
			('keyup'
				, function (event) {
					replacedValue = event.target.value.replace(/\D/g, '')
					if (event.target.value != replacedValue) {
						event.target.value = replacedValue
					}
					scope.$apply()
				}
			)
			elm.bind
			('keypress'
				, function (event) {
					event.keyCode = event.charCode || event.keyCode
					if
						(event.shiftKey						 // disallow Shift
						|| ( event.keyCode < 48			 // disallow non-numbers
						|| event.keyCode > 57
						)
						&& event.keyCode != 46			 // allow delete
						&& event.keyCode != 8				 // allow backspace
						&& event.keyCode != 9				 // allow not tab
						&& event.keyCode != 27			 // allow escape
						&& event.keyCode != 13			 // allow enter
						&& event.keyCode != 39			 // allow right arrow
						&& event.keyCode != 40			 // allow left arrow
						&& !( event.keyCode == 65		 // allow CTRL+A
						&& event.ctrlKey === true
						)
						&& !( event.keyCode == 67		 // allow CTRL+C
						&& event.ctrlKey === true
						)
						&& !( event.keyCode == 80		 // allow CTRL+P
						&& event.ctrlKey === true
						)
						) {
						event.preventDefault()
						scope.$apply()
					}
				}
			)
		}
		}
		return directive
	}
)


angular.module('app').directive
('onTap'
	, function () {
		var directive =
		{
			link: function (scope, element) {
				element.bind('touchstart click dblclick', function () {
					element.css('opacity', '0.8')
					setTimeout(function () {
						element.css('opacity', '1')
					}, 250)
				});

			}
		}
		return directive
	}
)


angular.module('app')
	.directive
('displayFooter'
	, function ($routeParams,enumsService) {
	 var directive =
	 { link: function (scope, element) {

		 // @TODO: Year should come from server config.
		 var d = new Date();
		 var year = d.getFullYear();

		 element.append('<p class="privacy">&copy; ' + year + ' Palms & Ponies. All Rights Reserved |' + 'website design by myphpdelights.com |'+
			                ' <a href="' + enumsService.routes.PRIVACY +'">Privacy Statement</a> </p>')
	 }
	 }

	 return directive
 }
)


angular.module('app').directive
('webKitTransitions'
	, function () {
		var directive =
		{
			link: function (scope, element) {
				element.removeClass('preload');
			}
		}
		return directive
	}
)

/**
 * Validate is ngModel given is equal to the property being tested
 * http://jsfiddle.net/pkozlowski_opensource/GcxuT/23/
 */
angular.module('app')
	.directive
('uiValidateEquals',
	function() {

	return {
		restrict: 'A',
		require: 'ngModel',
		link: function(scope, elm, attrs, ctrl) {

			function validateEqual(myValue, otherValue) {
				if (myValue === otherValue) {
					ctrl.$setValidity('equal', true);
					return myValue;
				} else {
					ctrl.$setValidity('equal', false);
					return undefined;
				}
			}

			scope.$watch(attrs.uiValidateEquals, function(otherModelValue) {
				validateEqual(ctrl.$viewValue, otherModelValue);
			});

			ctrl.$parsers.unshift(function(viewValue) {
				return validateEqual(viewValue, scope.$eval(attrs.uiValidateEquals));
			});

			ctrl.$formatters.unshift(function(modelValue) {
				return validateEqual(modelValue, scope.$eval(attrs.uiValidateEquals));
			});
		}
	};
});