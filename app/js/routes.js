/**
 * # User-facing URL Router
 *
 * Specified what URLs are available to a user, and what controllers should be
 * assigned to each.
 */
angular.module('app').config( function( $routeProvider , $locationProvider, $httpProvider ) {


		delete $httpProvider.defaults.headers.common['X-Requested-With'];

		$routeProvider
		.when
		(	'/'
		,	{ templateUrl: 'views/index.html'
			, controller: 'ApplicationController'
			}
		)
		.when
		(	'/registration'
		,	{ templateUrl: 'views/registration.html'
			, controller: 'RegistrationController'
		}
		)
			.when
		(	'/class'
			,	{ templateUrl: 'views/class.html'
				, controller: 'ApplicationController'
			}
		)
			.when
		(	'/privacy'
			,	{ templateUrl: 'views/privacy.html'
				 , controller: 'ApplicationController'
			 }
		)
			.when
		(	'/guidelines'
			,	{ templateUrl: 'views/guidelines.html'
				, controller: 'ApplicationController'
			}
		)
			.when
		(	'/wiki'
			,	{ templateUrl: 'views/wiki.html'
				, controller: 'ApplicationController'
			}
		)
			.when
		(	'/contact'
			,	{ templateUrl: 'views/contact.html'
				, controller: 'ContactController'
			}
		)
			.when
		(	'/about'
			,	{ templateUrl: 'views/construction.html'
				, controller: 'ContactController'
			}
		)
			.when
		(	'/class-saturday'
			,	{ templateUrl: 'views/class-saturday.html'
				, controller: 'ContactController'
			}
		)
			.when
		(	'/class-sunday'
			,	{ templateUrl: 'views/class-sunday.html'
				, controller: 'ContactController'
			}
		)
			.when
		(	'/breed'
			,	{ templateUrl: 'views/breed-assignment.html'
				, controller: 'ContactController'
			}
		)
			.when
		(	'/divisions'
			,	{ templateUrl: 'views/divisions.html'
				, controller: 'ContactController'
			}
		)
			.when
		(	'/entry'
			,	{ templateUrl: 'views/entry-info.html'
				, controller: 'ContactController'
			}
		)
			.when
		(	'/donate'
			,	{ templateUrl: 'views/donate.html'
				, controller: 'ContactController'
			}
		)


		/**
		 * ## HTML5 pushState support
		 *
		 * This enables urls to be routed with HTML5 pushState so they appear in a
		 * '/someurl' format without a page refresh
		 *
		 * The server must support routing all urls to index.html as a catch-all for
		 * this to function properly,
		 *
		 * The alternative is to disable this which reverts to '#!/someurl'
		 * anchor-style urls.
		 */
		$locationProvider.html5Mode(true)
	}
)


