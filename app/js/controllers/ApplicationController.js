/**
 * # ApplicationController
 *
 * Core application controller that includes all services needed to
 * kick-start the application
 */
angular.module('app').controller(
	'ApplicationController'
	,
	function
		(
			$rootScope
			, $scope
			, $log
			, $timeout
			, $location
			, $routeParams
			, $window
			) {

		$rootScope.siteName = 'SITE'



		$scope.height = window.innerHeight;
		$scope.userAgent = navigator.userAgent


		var isIPhone = $scope.userAgent.toLowerCase().indexOf("iphone");
		var isAndroid = $scope.userAgent.toLowerCase().indexOf("android");




		//HIDE THE ADDRESS BAR
		angular.element( $window ).bind
		( "orientationchange load"
			, function (){
			  $window.scrollTo(0, 1)

		  }
			, false
		)




		$scope.$on('$routeChangeSuccess', function(){
		})
		$scope.$on('$routeChangeStart', function(){
		})

	}
)
