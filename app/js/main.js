// Require JS  Config File
require({
		baseUrl: 'js/',
		paths: {
			 'angular': '../lib/angular/index'
			, 'angular-resource': '../lib/angular-resource/index'
			, 'jquery': '../lib/jquery/jquery'
			, 'prefixfree': '../lib/prefixfree/prefixfree'
		},
		shim: {
			'app': {
				'deps': [
					'angular'
					, 'angular-resource'
					//, 'prefixfree'
				]
			},
			'angular-resource': { 'deps': ['angular','jquery'], 'exports':'angular' },
			'routes': { 'deps': [
				'app'
			]},

			'controllers/ApplicationController': {
				'deps': [
					'app'
				]}
		}
	},
	[
		'require'
		, 'routes'
		, 'controllers/ApplicationController'

	],
	function (require) {
		return require(
			[
				'bootstrap'
			]
		)
	}
);
